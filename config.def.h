/* appearance */
static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 2;  /* border pixel of windows */
static const unsigned int gappih    = 10; /* horiz inner gap between windows */
static const unsigned int gappiv    = 10; /* vert inner gap between windows */
static const unsigned int gappoh    = 10; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10; /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;  /* 1 means no outer gap when there is only one window */
static const float rootcolor[]      = {0.09, 0.09, 0.13, 1.0};
static const float bordercolor[]    = {0.42, 0.44, 0.54, 1.0};
static const float focuscolor[]     = {0.52, 0.63, 0.78, 1.0};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	{ "firefox",  NULL,       1 << 8,       0,           -1 },
	*/
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors
 * The order in which monitors are defined determines their position.
 * Non-configured monitors are always added to the left. */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect x y */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0 },
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	.layout = "dvorak",
	.options = "caps:swapescape",
};

static const int repeat_rate = 25;
static const int repeat_delay = 600;

static const char *kblayouts[] = {"us(dvorak)", "fr(bepo_afnor)"};

/* Trackpad */
static const int tap_to_click = 1;
static const int natural_scrolling = 0;

#define MODKEY WLR_MODIFIER_LOGO
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, KEY,            tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,KEY,toggletag,  {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[] = { "kitty", NULL };
static const char *menucmd[] = { "bemenu-run", NULL };

#include "keys.h"
static const Key keys[] = {
	/* modifier                  key                function          argument */
	{ MODKEY,                    Key_d,             spawn,            {.v = menucmd} },
	{ MODKEY,                    Key_Return,        spawn,            {.v = termcmd} },
	{ MODKEY,                    Key_p,             spawn,            SHCMD("pass-bemenu") },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_p,             spawn,            SHCMD("pass-bemenu -u") },
	{ MODKEY|WLR_MODIFIER_CTRL,  Key_p,             spawn,            SHCMD("pass-bemenu -o") },
	{ 0,                         Key_AudioRaiseVol, spawn,            SHCMD("vol up") },
	{ 0,                         Key_AudioLowerVol, spawn,            SHCMD("vol down") },
	{ 0,                         Key_AudioMute,     spawn,            SHCMD("vol toggle") },
	{ 0,                         Key_AudioNext,     spawn,            SHCMD("music skip") },
	{ 0,                         Key_AudioPrev,     spawn,            SHCMD("music previous") },
	{ 0,                         Key_AudioPlay,     spawn,            SHCMD("music forward") },
	{ 0,                         Key_AudioStop,     spawn,            SHCMD("music backward") },
	{ MODKEY,                    Key_s,             spawn,            SHCMD("scrot") },
	{ MODKEY,                    Key_j,             focusstack,       {.i = +1} },
	{ MODKEY,                    Key_k,             focusstack,       {.i = -1} },
	{ MODKEY,                    Key_bracketright,  incnmaster,       {.i = -1} },
	{ MODKEY,                    Key_bracketleft,   incnmaster,       {.i = +1} },
	{ MODKEY,                    Key_h,             setmfact,         {.f = -0.05} },
	{ MODKEY,                    Key_l,             setmfact,         {.f = +0.05} },
	{ MODKEY,                    Key_z,             zoom,             {0} },
	{ MODKEY,                    Key_Tab,           view,             {0} },
	{ MODKEY,                    Key_x,             killclient,       {0} },
	{ MODKEY,                    Key_comma,         setlayout,        {.v = &layouts[0]} },
	{ MODKEY,                    Key_period,        setlayout,        {.v = &layouts[1]} },
	{ MODKEY,                    Key_slash,         setlayout,        {.v = &layouts[2]} },
	{ MODKEY|WLR_MODIFIER_CTRL,  Key_space,         setlayout,        {0} },
	{ MODKEY,                    Key_space,         togglekblayout,   {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_space,         togglefloating,   {0} },
	{ MODKEY,                    Key_f,             togglefullscreen, {0} },
	{ MODKEY,                    Key_w,             focusmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    Key_e,             focusmon,         {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_w,             tagmon,           {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_e,             tagmon,           {.i = WLR_DIRECTION_RIGHT} },
	TAGKEYS(                     Key_1,                               0),
	TAGKEYS(                     Key_2,                               1),
	TAGKEYS(                     Key_3,                               2),
	TAGKEYS(                     Key_4,                               3),
	TAGKEYS(                     Key_5,                               4),
	TAGKEYS(                     Key_6,                               5),
	TAGKEYS(                     Key_7,                               6),
	TAGKEYS(                     Key_8,                               7),
	TAGKEYS(                     Key_9,                               8),
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_x,             quit,             {0} },

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,Key_BackSpace, quit, {0} },
#define CHVT(KEY,n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT, KEY, chvt, {.ui = (n)} }
	CHVT(Key_F1, 1), CHVT(Key_F2,  2),  CHVT(Key_F3,  3),  CHVT(Key_F4,  4),
	CHVT(Key_F5, 5), CHVT(Key_F6,  6),  CHVT(Key_F7,  7),  CHVT(Key_F8,  8),
	CHVT(Key_F9, 9), CHVT(Key_F10, 10), CHVT(Key_F11, 11), CHVT(Key_F12, 12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
